#from gevent import monkey
#monkey.patch_all()
from flask import *
from httplib import *
import sqlite3
import os
import MySQLdb as mdb
from pyes import *
from pyes.facets import *
from collections import defaultdict
from datetime import datetime

backdate = datetime.strptime('01012014', "%d%m%Y")  

con = None
es_conn = None
server_name = "lsir-cluster-01.epfl.ch:9200"
index_name = "tweets_original_date"
mapping_name = "mapping_tweet_downloaded_date"
app = Flask(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
app.config.update(
  DEBUG=True,
  SQLCON = None,
  ES_HOST = 'lsir-cluster-01.epfl.ch',
  ES_PORT = 9200,
  ES_CONN = None,
  URL_TIMELINE = '/tweets_original_date/_search',
  FACET_TIMELINE = '''{
  "facets": {
    "1": {
      "date_histogram": {
        "field": "created_at",
        "interval": "2d"
      },
      "facet_filter": {
        "fquery": {
          "query": {
            "filtered": {
              "query": {
                "query_string": {
                  "query": "*"
                }
              },
              "filter": {
                "bool": {
                  "must": [
                    {
                      "match_all": {}
                    },
                    {
                      "bool": {
                        "must": [
                          {
                            "match_all": {}
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      }
    }
  },
  "size": 0
}'''
  )
  
#@app.errorhandler(404)
#def page_not_found(error):
#  #return send_from_directory(app.static_folder, 'html/404.html'), 404
#  return app.send_static_file('html/404.html'), 404

def facetHandler(url, query, respPath, titles=None):
	"""Execute facet query and convert results for drawing

	url - target facet url on ES
	query - facet query string in JSON format
	respPath - response path to fetch target data
	titles - array of titles to supply to the client

	returns JSON of facet execution results converted to the representation
		suitale for drawing by the client: array of 2 arrays: titles and
		corresponding datasets
	"""
	#if not url or not query:
	#	raise InvalidParam('...')
	# Init connection if required and execute the query
	conn = app.config['ES_CONN']
	if not conn:
		conn = HTTPConnection(app.config['ES_HOST'], app.config['ES_PORT'])
		app.config['ES_CONN'] = conn
		#httpcon.putrequest('GET', 'http://<website>/ocd/')
		#httpcon.putheader('Connection','Keep-Alive')
		#httpcon.endheaders()	try:
	try:
		conn.request('GET', app.config[url], app.config[query])
	except HTTPException:
		conn = HTTPConnection(app.config['ES_HOST'], app.config['ES_PORT'])
		app.config['ES_CONN'] = conn
		conn.request('GET', app.config[url], app.config[query])
	# Get responce form ES
	resp = json.loads(conn.getresponse().read())['facets']
	# Fetch target data
	for e in respPath:
		resp = resp[e]
	# Convert to representation for drawing
	if not titles:
		titles = resp[0].keys() if resp else []
	data = [titles, [e.values() for e in resp]]
	return json.dumps(data, separators=(',',':'))

#@app.route('/api/topusers/')
#def topusers():
#	return facetHandler('URL_TOPUSERS', 'FACET_TOPUSERS', ['terms', 'terms'], ['count', 'user'])

@app.route('/api/timeline/')
def timeline():
	return facetHandler('URL_TIMELINE', 'FACET_TIMELINE', ['1', 'entries'])


from functools import wraps

def memo(func):
    cache = {}
    @wraps(func)
    def wrap(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrap

#@app.route('/',defaults={'keyword':None})
#@app.route('/<keyword>')
#def get_data(keyword):


def test_mysql_con():
    global con
    try:
         cur = con.cursor()
         cur.execute("SELECT VERSION()")
    except Exception, e:
        con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
        con.autocommit(True)


def test_es_con():
    global es_conn
    try:
      status = es_conn.status()
    except Exception, e:  
      es_conn = ES(server_name,bulk_size=500)


@memo
def get_stories_db_data(keyword):
  cur = con.cursor(mdb.cursors.DictCursor)
  cur.execute("select * from stories")
  stories = cur.fetchall()
  return stories

@memo
def get_people_db_data(keyword):
    cur = con.cursor(mdb.cursors.DictCursor)
    cur.execute("select * from users limit 10")
    entries = cur.fetchall()
    return entries

@memo
def get_topics_db_data(keyword):
  cur = con.cursor(mdb.cursors.DictCursor)
  cur.execute("select * from topics")
  topics = cur.fetchall()
  return topics


@memo
def get_tweets_db_data(keyword):
  cur = con.cursor(mdb.cursors.DictCursor)
  cur.execute("select * from tweets")
  topics = cur.fetchall()
  return topics


@memo
def get_debates_db_data(keyword):
    cur = con.cursor(mdb.cursors.DictCursor)
    cur.execute("select * from debates")
    entries = cur.fetchall()
    return entries

@memo
def get_debates_arguments_db_data(keyword):
    cur = con.cursor(mdb.cursors.DictCursor)
    cur.execute("select * from debate_arguments")
    entries = cur.fetchall()
    return entries


@app.route('/')
@app.route('/search/',defaults={'keyword':None})
@app.route('/search/<keyword>')
def get_data(keyword=None):
  #return 'Hello World!'
  global con
  if not con:
    con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
    con.autocommit(True)

  #stories_db_data = get_stories_db_data(keyword)
  #people_db_data = get_people_db_data(keyword)
  debates_db_data = sorted(get_debates_db_data(keyword),key = lambda x : -1 * x['Id'])
  topics_db_data = get_topics_db_data(keyword)
  debates_arguments_data = get_debates_arguments_db_data(keyword)
  tweets_data = get_tweets_db_data(keyword)
  tt_ids = {}
  for i,tw in enumerate(tweets_data):
      tt_ids[tw['topic_id']] = i  
  
  #values = [['users', 'frequency'], [['anonimous93', 90], ['mkunt4', 80], ['globalgrind', 70], ['greenliberation', 85]]];
  #return render_template('index.html',stories=stories_db_data,values=values,people=people_db_data,debates=debates_db_data,debate_arguments=debates_arguments_data,topics=topics_db_data)
  return render_template('index.html',debates=debates_db_data,debate_arguments=debates_arguments_data,topics=topics_db_data,tweets=tweets_data,tt_ids=tt_ids)
  #return reirect(app.static_folder + 'index.html', code=302)

@memo
def get_db_data(sql_query):
  test_mysql_con()
  cur = con.cursor(mdb.cursors.DictCursor)
  cur.execute(sql_query)
  entries = cur.fetchall()
  cur.close()
  return entries

@memo
def get_topic_keywords(topic_id):
	topic_keywords = get_db_data("select keyword from topics_keywords as sk, keywords as k where sk.topic_id = " + str(topic_id) +  " and sk.keyword_id = k.id ") if topic_id is not None else get_db_data("select keyword from topics_keywords as sk, keywords as k where sk.keyword_id = k.id")
	return topic_keywords

@memo
def get_story_keywords(story_id):
  story_keywords = get_db_data( "select keyword from stories_keywords as sk, keywords as k where sk.story_id = " + str(story_id) +  " and sk.keyword_id = k.id ") 
  return story_keywords

## assumes k is a string and v is a number
def convert_dict_to_string(dictv, key1 = lambda x : x[0]):
    ans = "["
    list_dict = sorted(dictv.items(),key = key1)
    for k,v in list_dict:
      ans += "[\"" + k + "\"," + str(v) + "],"
    return ans[0:-1] + "]"  

#@app.route('/facet/stories_user_info',defaults={'keyword':None})
#@app.route('/facet/stories_user_info/<keyword>')
def get_stories_user_info(keyword):
    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    ans_dict = {}
    stories= get_stories_db_data(keyword)
    for story in stories:
      keywords = get_story_keywords(story['Id'])
      keyword_list = [x['keyword'] for x in keywords]
      user_type_count = get_user_type_stats(keyword_list)

      ans_dict[story['Id']] = "[[\"users\", \"frequency\"], " +  convert_dict_to_string(user_type_count)  + "]"
    return jsonify(ans_dict)  

@app.route('/api/topic_sentiments/', defaults={'tid': 0})
@app.route('/api/topic_sentiments/<int:tid>')
def get_topic_sentiments(tid):
    global con
    if not con:
      con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
      con.autocommit(True)

    ans_dict = {}
    if not tid:
        tid = None
    keywords = get_topic_keywords(tid)
    keyword_list = [x['keyword'] for x in keywords]
    sentiment_count = get_sentiment_stats(keyword_list)

    data = [["sentiments", "opinion"], sentiment_count]
    data = json.dumps(data, separators=(',',':'))
    ans_dict[tid] = data
    #return jsonify(ans_dict)  
    return data


@app.route('/api/topic_topics/', defaults={'tid': 0})
@app.route('/api/topic_topics/<int:tid>')
def get_topic_topics(tid):
    global con
    if not con:
      con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
      con.autocommit(True)

    ans_dict = {}
    if not tid:
        tid = None
    keywords = get_topic_keywords(tid)
    keyword_list = [x['keyword'] for x in keywords]
    topic_count = get_topic_keyword_stats(keyword_list)

    #data = json.dumps(data, separator=(',',':'))
    tc = []
    for k,v in topic_count.items():
        tc.append([k,v])
        
    data = [["topics", "opinion"], tc]
    #ans_dict[tid] = data
    #return jsonify(ans_dict)  
    return json.dumps(data)


@app.route('/api/stories_sentiment_info',defaults={'keyword':None})
@app.route('/api/stories_sentiment_info/<keyword>')
def get_stories_sentiment_info(keyword):
    global con
    if not con:
      con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
      con.autocommit(True)

    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    ans_dict = {}
    stories= get_stories_db_data(keyword)
    for story in stories:
      keywords = get_story_keywords(story['Id'])
      keyword_list = [x['keyword'] for x in keywords]
      sentiment_count = get_sentiment_stats(keyword_list)

      ans_dict[story['Id']] = "[[\"sentiments\", \"opinion\"], " +  convert_dict_to_string(sentiment_count)  + "]"
    return jsonify(ans_dict)  



@app.route('/api/topics_user_info',defaults={'tid':0})
@app.route('/api/topics_user_info/<int:tid>')
def get_topics_user_info(tid):
    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    global con
    if not con:
      con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
      con.autocommit(True)

    ans_dict = {}
    if not tid:
        tid = None
    keywords = get_topic_keywords(tid)
    keyword_list = [x['keyword'] for x in keywords]
    tc = get_user_type_stats(keyword_list)

    tc2 = []
    for k,v in tc.items():
        tc2.append([k,v])
        
    data = [["topics", "opinion"], tc2]
    #ans_dict[tid] = data
    #return jsonify(ans_dict)  
    return json.dumps(data) 

@app.route('/api/topics_sentiment_info',defaults={'keyword':None})
@app.route('/api/topics_sentiment_info/<keyword>')
def get_topics_sentiment_info(keyword):
    global con
    if not con:
      con = mdb.connect('lsir-cluster-01.epfl.ch' , 'climate_change2', 'ccA123', 'climate_change_db2')
      con.autocommit(True)

    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    ans_dict = {}
    topics= get_topics_db_data(keyword)
    for topic in topics:
      keywords = get_topic_keywords(topic['Id'])
      keyword_list = [x['keyword'] for x in keywords]
      sentiment_count = get_sentiment_stats(keyword_list)

      ans_dict[topic['Id']] = "[[\"sentiments\", \"opinion\"], " +  convert_dict_to_string(sentiment_count)  + "]"
    return jsonify(ans_dict)  

#@app.route('/facet/people_topic_info',defaults={'keyword':None})
#@app.route('/facet/people_topic_info/<keyword>')
def get_people_topic_info(keyword):
    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    ans_dict = {}
    people = get_people_db_data(keyword)
    for person in people:
      topic_stats = get_topic_stats(person['screenname'])
      print topic_stats
      ans_dict[person['Id']] = "[[\"topic\", \"opinion\"], " +  convert_dict_to_string(topic_stats,key1=lambda x:x[1])  + "]"
    print ans_dict
    return jsonify(ans_dict)  


#@app.route('/facet/people_sentiment_info',defaults={'keyword':None})
#@app.route('/facet/people_sentiment_info/<keyword>')
def get_people_sentiment_info(keyword):
    #return jsonify ({"a" :"[[\"users\", \"frequency\"], [[\"anonimous93\", 9128], [\"mkunt4\", 8751], [\"globalgrind\", 5050]]]"});
    ans_dict = {}
    people = get_people_db_data(keyword)
    for person in people:
      sent_stats = get_user_sentiment_stats(person['screenname'])
      print sent_stats
      ans_dict[person['Id']] = "[[\"topic\", \"opinion\"], " +  convert_dict_to_string(sent_stats,key1=lambda x:x[1])  + "]"
    print ans_dict
    return jsonify(ans_dict)


#@app.route('/facet/story_users',defaults={'keyword':None})
#@app.route('/<keyword>')
#def get_stories_users_data(keyword):
  

#@app.route('/charts_stories.js')
def chart():
  data = [['users', 'frequency'], [['anonimous93', 90], ['mkunt4', 80], ['globalgrind', 70], ['greenliberation', 85]]];
  print data
  return render_template('charts.js',values=data)

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db
   
def get_topic_keyword_stats(keywords):
    queries = [ TextQuery('text',kw) for kw in keywords]
    qb = BoolQuery(should=queries).search()      
    qb.facet.add_term_facet('topic',size=3)
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    topic_stats = rf.values()[0]['terms']
    topic_dict = {}
    for stat in topic_stats:
        topic_dict[stat['term']] = stat['count']
    
    return topic_dict   

def get_sentiment_stats(keywords):
    queries = [ TextQuery('text',kw) for kw in keywords]
    qb = BoolQuery(should=queries).search()    
    qb.facet.add(facets.StatisticalFacet("pos",field="positive_sentiment"))
    qb.facet.add(facets.StatisticalFacet("neg",field="negative_sentiment"))
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    pos_sen = min(100,int(rf['pos']['mean']*100))
    #print "rf[pos]: ", rf['pos'], keywords
    neg_sen = -1*min(100,int(rf['neg']['mean']*100))
    #return {"+ve" : pos_sen , "-ve" : neg_sen}  
    return [["+ve", pos_sen], ["-ve", neg_sen]]  

@memo
def get_user_sentiment_stats(username):
    query = TextQuery('user.screen_name',username,'phrase')
    qb = query.search()    
    qb.facet.add(facets.StatisticalFacet("pos",field="positive_sentiment"))
    qb.facet.add(facets.StatisticalFacet("neg",field="negative_sentiment"))
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    pos_sen = rf['pos']['total']
    neg_sen = -1*rf['neg']['total']
    return {"+ve" : pos_sen , "-ve" : neg_sen}  

@memo
def get_topic_stats(username):
    query = TextQuery('user.screen_name',username,'phrase')
    qb = query.search()    
    qb.facet.add_term_facet('topic',size=3)
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    topic_stats = rf.values()[0]['terms']
    topic_dict = {}
    for stat in topic_stats:
        topic_dict[stat['term']] = stat['count']
    
    return topic_dict

def replace_time(tweet):
    b = datetime.strptime(tweet['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
    x = (b - backdate).days
    tweet['created_at'] = b.date().strftime("%s000")
    if x < 0:
      return
    else:
      return tweet          

@app.route('/api/debate_timeline',defaults={'keyword':None})
@app.route('/api/debate_timeline/<keyword>')
def get_debate_timeline(keyword):
    data = []
    print keyword
    
    q1 = RangeQuery(qrange=ESRange("debate_storm",from_value=-1,to_value=-1))
    test_es_con()
    r1 = es_conn.search(q1)
    docs1 = [x for x in r1]
    print "AAA" + str(len(docs1))
    q2 = RangeQuery(qrange=ESRange("debate_storm",from_value=1,to_value=1))
    r2 = es_conn.search(q2)
    docs2 = [x for x in r2]
    print "BBB" + str(len(docs2))
    
    docs1 = [ replace_time(a) for a in docs1]
    docs2 = [ replace_time(a) for a in docs2]
    docs1 = [ x for x in docs1 if x]
    docs2 = [ x for x in docs2 if x]

    docs1 = sorted(docs1, key = lambda x : x['created_at'])
    docs2 = sorted(docs2, key = lambda x : x['created_at'])

    
    for_dict = defaultdict(float)
    against_dict = defaultdict(float)

    for i,doc in enumerate(docs1):
      against_dict[doc['created_at']] += 1.0

    for i,doc in enumerate(docs2):
      for_dict[doc['created_at']] += 1.0
    
    unif_keys = sorted(list(set(for_dict.keys() + against_dict.keys())))
    for k in unif_keys:
      a= None
      f = None  
      if k in against_dict:
          a = against_dict[k]
      if k in for_dict:
          f = for_dict[k]
      print k + "," + str(a) + "," + str(f)   
      data.append([int(k),a,f])
        

    print data
    return json.dumps([ ["time", "against", "for"],data])
        


def get_top_users(keywords):
    cur = con.cursor(mdb.cursors.DictCursor)
    cur.execute("select screenname , user_type from users")
    entries = cur.fetchall()
    dict_users = {}
    for user in entries:
        dict_users[user['screenname']] = user['user_type']

    queries = [ TextQuery('text',kw) for kw in keywords]
    qb = BoolQuery(should=queries).search()    
    qb.facet.add_term_facet('user.screen_name',size=2000000)
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    user_stats = rf.values()[0]['terms']
    total_count= 0
    users_list = defaultdict(int)
    for stat in user_stats:

        if stat['term'] in dict_users:
          users_list[stat['term']] = stat['count']
    
    users_list_items = sorted(users_list.items(), key = lambda x: -1*x[1])
    ans = [x[0] for x in users_list_items]      
    return ans[0:10]




def get_user_type_stats(keywords):
    cur = con.cursor(mdb.cursors.DictCursor)
    cur.execute("select screenname , user_type from users")
    entries = cur.fetchall()
    dict_users = {}
    for user in entries:
        dict_users[user['screenname']] = user['user_type']

    queries = [ TextQuery('text',kw) for kw in keywords]
    qb = BoolQuery(should=queries).search()    
    qb.facet.add_term_facet('user.screen_name',size=2000000)
    test_es_con()
    resultset = es_conn.search(qb,indices = index_name,types =mapping_name)
    rf = resultset.facets
    user_stats = rf.values()[0]['terms']
    total_count= 0
    user_type_count = defaultdict(int)
    for stat in user_stats:

        if stat['term'] in dict_users:
          user_type_count[dict_users[stat['term']]] += stat['count']
       
    return user_type_count      
        

def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

#@app.route('/')
def show_entries():
  
  cur = con.cursor(mdb.cursors.DictCursor)
  cur.execute('select heading,photo from debates order by id desc')
  entries = cur.fetchall()
  return entries

#@app.route('/')
#def index():
#	return render_template('index.html')

if __name__ == '__main__':
  #show_entries()
  test_mysql_con()
  test_es_con()
  get_user_type_stats(["oil"])
  print "AAAAA"
  app.run(app.config.get('SERVER_HOST'), app.config.get('SERVER_PORT'))
    
  #app.config['ES_CONN'] = HTTPConnection(app.config['ES_HOST'], app.config['ES_PORT'])
