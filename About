Distributed Information Systems Laboratory, in collaboration with EPFL Social Media Lab and Science Po médialab, 
has been working on the problem of understanding and facilitating public debate. In particular, we have been studying 
discussions on Twitter related to the climate change, and extracting from the unstructured tweets structured information
such as topics, issues, opinions, and arguments. Subsequently, we attempt to understand how and why these elements change
over time. The goal of our research is to learn how to automatically analyze public opinion, expose bias, determine credible 
proof, and drive informal exchanges towards a consensus.

This analysis will go well beyond sentiment and topic extraction to cover such complex elements as issues, causes, arguments, 
and evidence. Moreover, our work will cover several popular languages and will include a state-of-the-art credibility analysis.

The source code in this repository is available under the GNU General Public License version 3.0 (GPL-3.0).