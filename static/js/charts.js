/** @title Charts
 * @author Artem V L, mailto: lav@lumais.com
 * @date: 2014-01-07
 * Made for ScienceWise
*/
"use strict";

/** Incapsulates drawing of all charts */
function ClimoCharts() {
	/** Receives JSON data and processes it in a specified way
	 * url  - url to reeive data from, string
	 * handler - target handler, function(data), data is JSON string
	 * timeout - receive timeout in sec.. Optional, default: 50
	 */
	function JsonHandler(url, handler, id, a1, a2, timeout) {
		if(!url || typeof url != "string" || !handler || typeof handler != "function"
		|| timeout != null && typeof timeout != "number")
			throw "Invalid input data: url must be valid string, chart - function, timeout - number";

		this.handler = handler;  // Data handler that draws it
		this.timeout = timeout || 50;  // sec.
		this.data = null;  // Data received from the specified URL
		this.id = id;
		this.a1 = a1;
		this.a2 = a2;

		/** Data receive callback
		 * err - data receiving error
		 * data - received data
		 * Note: assign to nonlocal data
		 */
		Object.defineProperty(this, "onReceive", {
			value: function(err, data) {
				if(err)
					throw "JSON receive failed: " + err;
				this.data = data;  // Nonlocal assign
			}
		});
		//function onReceive (err, d) {
		//	if(err)
		//		throw "JSON receive failed: " + err;
		//	data = d;  // Nonlocal assign
		//}

		/** Wait for data receive and execute the handler
		 * Note: uses nonlocal vars: data, timeout, handler
		 */
		Object.defineProperty(this, "waitReceive", {
			value: function() {
				if(!this.data) {
					if(this.timeout) {
						--this.timeout;
						setTimeout(this.waitReceive.bind(this), 1000);
					} else {
						//notifyUser();  // Hide notification window
						//setTimeout(notifyUser.bind(null, "Data was not received from the server, timeout expired"), animTime / 1.8);
						//alert("Error: Data was not received from the server to build the graph, timeout expired");
						throw "Data receiving timeout expired";
					}
				} else this.handler(this.data, this.id, this.a1, this.a2);
			}
		});

		///** Wait for data receive and execute the handler
		// * handler - handler to be executed, function(data), data is JSON string
		// * data - data to be processed
		// * timeout - time limit for data receiving, sec.
		// */
		//function waitReceive(handler, data, timeout) {
		//	if(!data) {
		//		if(timeout) {
		//			--timeout;
		//			setTimeout(waitReceive.bind(null, handler, data, timeout), 1000);
		//		} else {
		//			//notifyUser();  // Hide notification window
		//			//setTimeout(notifyUser.bind(null, "Data was not received from the server, timeout expired"), animTime / 1.8);
		//			//alert("Error: Data was not received from the server to build the graph, timeout expired");
		//			throw "Data receiving timeout expired";
		//		}
		//	} else handler(data);
		//}

		// Process -------------------------------------------------------------
		// Load JSON data
		//notifyUser("Receiving the data ...");
		d3.json(url, this.onReceive.bind(this));

		// Draw Chart when data is ready or throw exception
		this.waitReceive(handler);
	}

	// Charts ==================================================================
	// Accessory Charts routines -----------------------------------------------
	// Main Charts routines ----------------------------------------------------
	/** Draw horizontal score bar
	 * pos - pos rate
	 * neg - neg rate
	 * id - element id
	 */
	this.drawHorizScoreBar = function(pos, neg, id) {
		// Validate input data
		if(typeof pos != "number" || typeof neg != "number" || pos < 0 || neg < 0)
			throw "Invalid put data: pos, neg must be nonnegative numbers"

		// Select DOM chart stub and use it
		var chart = d3.select(id);
		if(!chart)
			throw "Invalid input data: element ".concat(id, " is not exist");
		// Remove previous visualization
		chart.select("div").remove();

		var width = chart.node().clientWidth || chart.node().parentNode.clientWidth;
		//var height = chart.node().clientHeight || chart.node().parentNode.clientHeight;
		var wpos = pos && Math.round(pos * width / (pos + neg));

		chart.append("div")
			.style({width: wpos + "px"});
	}

	/** Draw score pie
	 * pos - pos rate
	 * neg - neg rate
	 * id - element id
	 */
	this.drawScorePie = function(pos, neg, id) {
		// Validate input data
		if(typeof pos != "number" || typeof neg != "number" || pos < 0 || neg < 0)
			throw "Invalid put data: pos, neg must be nonnegative numbers"

		// Select DOM chart stub and use it
		var chart = d3.select(id);
		if(!chart)
			throw "Invalid input data: element ".concat(id, " is not exist");
		// Remove previous visualization
		chart.select("g").remove();

		//var width = chart.node().clientWidth || chart.node().parentNode.clientWidth;
		//var height = chart.node().clientHeight || chart.node().parentNode.clientHeight;
		var norm = 100;  // Normalization value [0 .. norm]
		var r1 = 23, r2 = 30;
		var nneg = neg && Math.round(neg * norm / (pos + neg));  // Normalized negative
		var scale = d3.scale.linear().domain([0, norm]).range([0, 2 * Math.PI]);

		var arc = d3.svg.arc()
			.innerRadius(r1)
			.outerRadius(r2)
			.startAngle(function(d) {return scale(d[0]);})
			.endAngle(function(d) {return scale(d[1]);});

		var data = [[0, nneg, "neg"], [nneg, norm, "pos"]];
		chart = chart.append("g");
		chart.selectAll("path").data(data).enter().append("path")
			.attr({d: arc, class: function(d) {return d[2];}
				, transform: "translate(".concat(r2, ", ", r2, ")")})
				;
		chart.append("text")
			.attr({x: 0, y: "0.35em", "text-anchor": "middle"
				, transform: "translate(".concat(r2, ", ", r2, ")")})
			.text(pos + neg)
			;
	}
	
	/** Draw simple horizontal bar
	 * data  - input data, array of 2 arrays: titles and datasets
	 * id  - target HTML element (svg) id
	 * iCap  - index of data-caption
	 * iVal  - index of data-value
	 * stripeHeight  - stripe height in px. Optional, default 20
	 */
	this.drawHorizSimpleBar = function(data, id, iCapt, iVal, stripeHeight) {
		// Define optional values
		if(stripeHeight == null)
			stripeHeight = 20;
		
		// Validate input parameters
		if(!(data instanceof Array) || !(data[1] instanceof Array)
		|| data[0].length != data[1][0].length)
			throw "Invalid put data, it must be array of 2 arrays: titles and datasets and must be consistent";

		// Skip data titles
		data = data[1];
		var itemSize = data[0].length;  // Data item size in number of fields

		if(typeof stripeHeight != "number" || stripeHeight <= 0
		|| typeof iCapt != "number" || iCapt < 0 || iCapt >= itemSize
		|| typeof iVal != "number" || iVal < 0 || iVal >= itemSize
		)
			throw "Invalid type/range of input data: iCapt, iVal, stripeHeight must be positive numbers; iCapt, iVal < data.length";

		// Select DOM chart stub and use it
		var chart = d3.select(id);
		if(!chart)
			throw "Invalid input data: element ".concat(id, " is not exist");
		chart.selectAll("div").remove();

		var width = chart.node().clientWidth || chart.node().parentNode.clientWidth;
		var height = data.length * stripeHeight;

		// Set chart height
		chart.attr("height", height);

		var /* width = 700,  /** Chart wide, px */
			vmax = data.reduce(function maxColVal(vmax, e) {
					var val = e[iVal];
					if(vmax < val)
						vmax = val;
					return vmax;
				}, 0),
			scale = d3.scale.linear()
				.domain([0, vmax])  // d3.max(data[1])
				.range([0, width]);

		// Draw the chart
		chart.selectAll("div")
			.data(data)
		.enter().append("div")
			.style("width", function(d) { return scale(d[iVal]) + "px"; })
			.text(function(d) { return d[iCapt].concat(": ", d[iVal]); });
	}

	/** Draw horizontal bar with positive-negative values
	 * data  - input data, array of 2 arrays: titles and datasets
	 * id  - target HTML element (svg) id
	 * iCap  - index of data-caption
	 * iVal  - index of data-value
	 * stripeHeight  - stripe height in px. Optional, default 20
	 */
	this. drawHorizPosNegBar = function(data, id, iCapt, iVal, stripeHeight) {
		// Define optional values
		if(stripeHeight == null)
			stripeHeight = 20;
		
		// Validate input parameters
		if(!(data instanceof Array) || !(data[1] instanceof Array)
		|| data[0].length != data[1][0].length)
			throw "Invalid put data, it must be array of 2 arrays: titles and datasets and must be consistent";

		// Skip data titles
		data = data[1];
		var itemSize = data[0].length;  // Data item size in number of fields

		// Validate input parameters
		if(typeof stripeHeight != "number" || stripeHeight <= 0
		|| typeof iCapt != "number" || iCapt < 0 || iCapt >= itemSize
		|| typeof iVal != "number" || iVal < 0 || iVal >= itemSize
		)
			throw "Invalid type/range of input data: iCapt, iVal, stripeHeight must be positive numbers; iCapt, iVal < data.length";

		var svg = d3.select(id);
		if(!svg)
			throw "Invalid input data: element ".concat(id, " is not exist");
		svg.selectAll("g").remove();

		var width = svg.node().clientWidth || svg.node().parentNode.clientWidth;
		var height = data.length * stripeHeight;

		// Set svg height
		svg.attr("height", height);

		// Prepare x, y scale
		var drange = d3.extent(data, function(d) { return d[iVal]; })
		var x = d3.scale.linear()
			.domain(drange)
			.range([0, width]);
		var y = d3.scale.ordinal()
			.domain(data.map(function(d) { return d[iCapt]; }))
			.rangeRoundBands([0, height], .1);

		//console.log("y.rangeBand(): ", y.rangeBand());

		// Draw the chart
		//svg.append("g").append("rect")
			//.attr("class", function(d) { return data[0][iVal] < 0 ? "pnbar negative" : "pnbar positive"; })
			//.attr("x", function(d) { return x(Math.min(0, data[0][iVal])); })
			//.attr("y", function(d) { return y(data[0][iCapt]); })
			//.attr("width", function(d) { return Math.abs(x(data[0][iVal]) - x(0)); })
			//.attr("height", y.rangeBand());

		var cbar = svg.append("g").selectAll(".pnbar").data(data).enter().append("g");
		cbar.append("rect")
			.attr("class", function(d) { return d[iVal] < 0 ? "pnbar negative" : "pnbar positive"; })
			.attr("x", function(d) { return x(Math.min(0, d[iVal])); })
			.attr("y", function(d) { return y(d[iCapt]); })
			.attr("width", function(d) { return Math.abs(x(d[iVal]) - x(0)); })
			.attr("height", y.rangeBand());
		cbar.append("text")
			.text(function(d) { return d[iCapt]; })
			.attr("dx", function(d) { return "".concat(x(Math.min(0, d[iVal])) + 4, "px"); })
			.attr("dy", function(d) { return "".concat(y(d[iCapt]) + stripeHeight * 2 / 3, "px"); });
			

		// Form axis ticks vals
		var tvals = [];
		var nticks = 10;
		var dval = drange[1] - drange[0];
		for(var i = 0; i < nticks; ++i)
			tvals.push(drange[0] + dval * i / nticks);
			
		// Draw Axises
		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("top")
			.ticks(nticks)
			.tickValues(tvals)
			.tickFormat(d3.format(",.0f"))
			;
		svg.append("g")
			.attr("class", "axis")
			.call(xAxis);

		svg.append("g")
			.attr("class", "axis").append("line")
				.attr("x1", x(0))
				.attr("x2", x(0))
				.attr("y2", height);
	}

	/** Draw world map
	 * data  - input data, array of 2 arrays: titles and datasets
	 * id  - target HTML element (svg) id
	 //* iCap  - index of data-caption
	 //* iVal  - index of data-value
	 //* stripeHeight  - stripe height in px. Optional, default 12
	 */
	function drawWorldMap(data, id) {  //, iCapt, iVal, stripeHeight) {
	}

	/** Draw time series chart
	 * data  - input data, array of 2 arrays: titles and datasets
	 * id  - target HTML element (svg) id
	 //* height  - stripe height in px. Optional, default 20
	 */
	this.drawTimeSeries = function(data, id) {
		// Validate input parameters
		if(!(data instanceof Array) || !(data[1] instanceof Array)
		|| data[0].length != data[1][0].length
		)
			throw "Invalid put data, it must be array of 2 arrays: titles and datasets and must be consistent";

		// Skip data titles
		var axisCapt = data[0];
		data = data[1];
		var itemSize = data[0].length;  // Data item size in number of fields

		var svg = d3.select(id);
		if(!svg)
			throw "Invalid input data: element ".concat(id, " is not exist");
		svg.select("g").remove();

		// Axes captions visualization constants, depends on font family and size,
		// but can't be evaluated in runtime
		var xCaptHeight = 36,  // Height of the x axis caption, px
			xCaptStep = 64,  // Step between tick on x axis, px
			digWidth = 8;  // Digit width to evaluate caption width of the y axis, px
			
		// Evaluate chart width and height considering axes captions
		var trange = d3.extent(data, function(d) { return d[0]; });
		var vranges = [];
		for(var k = 1; k < data[0].length; ++k)
             vranges.push(d3.extent(data, function(d, j) { return d[k] ||
                 (function nearest() {
                     var i = j;
                     var val = null;
 
                     while(--i >= 0 && val == null)
                         val = data[i][k];
                     i = j;
                     while(++i < data.length && data[i][k] == null);
                     if(i < data.length)
                         val = (val + data[i][k]) / 2;
                     return data[j][k] = val;
                 })();
             }));
/*
		for(var i = 1; i < data[0].length; ++i)
			vranges.push(d3.extent(data, function(d) { return d[i]; }));
		//var vranges = [d3.extent(data, function(d) { return d[1]; })];
*/
		var vrangeMax = vranges.reduce(function maxRange(best, cur) {
			if(cur[0] < best[0])
				best[0] = cur[0];
			if(cur[1] > best[1])
				best[1] = cur[1];
			return best;
		});
		var yCaptWidth = Math.log(vrangeMax[1]) / Math.log(10) * digWidth
		var width = parseInt(svg.style("width")) - yCaptWidth,
//(svg.node().clientWidth || svg.node().parentNode.clientWidth) - yCaptWidth,
			height = parseInt(svg.style("height")) - xCaptHeight;
//(svg.node().clientHeight || svg.node().parentNode.clientHeight) - xCaptHeight;

		// Scale values
		var x = d3.time.scale()
			.domain(trange)
			.range([0, width])
			;
		var y = d3.scale.linear()//log()
			.domain(vrangeMax)
			.range([height, 0])
			;

		// Evaluate time ticks considering chart width
		var nticks = Math.round(width / xCaptStep);
		var tvals = [];
		var dt = trange[1] - trange[0];
		for(var i = 0; i < nticks; ++i)
			tvals.push(new Date(trange[0] + Math.round(dt * i / nticks)));

		// Define axes values
		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom")
			.ticks(nticks)
			.tickValues(tvals)
			.tickFormat(d3.time.format("%y-%b-%d"))
			;
		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			//.tickFormat(d3.format(",.0f"))
			;
		var lines = [];
		vranges.forEach(function(vr, i) {
			lines.push(d3.svg.line()
				.x(function(d) { return x(d[0]); })
				.y(function(d) { return y(d[i + 1]); }));
		});
//		lines.push(d3.svg.line()
//			.x(function(d) { return x(d[0]); })
//			.y(function(d) { return y(d[1]); }));

		svg = svg.append("g")
			.attr("transform", "translate(".concat(yCaptWidth, ",0)"))
			;
		// Background rect is required to capture mouse events
		svg.append("rect")
			.attr({x: 0, y: 0, width: width, height: height
				, fill: "#FFF", opacity: 0})
			;
		// Marker and corresponding label of the pointing data
		var marker = svg.append("line")
			.style('opacity', 0)
			.attr({class: "marker", y1: 0, y2: height})
			;
		//var curval = svg.append("text")
			//.attr({class: "label", x: width - digWidth, y: "1em"})
			//.attr("text-anchor", "end")
			//;
		var curvals = [];
		var textDy = 1.2;
		for(var i = 1; i < axisCapt.length; ++i)
			curvals.push(svg.append("text")
				.attr({class: "label s" + i, x: width - digWidth, y: (1 + textDy * (i - 1)) + "em"})
			.attr("text-anchor", "end")
			);
		
		var curtime = svg.append("text")
			.attr({class: "label", x: width - digWidth, y: "1em", dy: textDy * (axisCapt.length - 1) + "em"})
			.attr("text-anchor", "end")
			;

		/** Define array items with closest value
		 * time - target value
		 * iTime - index of the comparing value in the array item (it's also array)
		 * return array item with the closest target value to specified one
		 */
		function closest(time, iTime) {
			var ib = 0,
				ie = data.length;
			
			while(ib !== ie) {
				var ic = Math.floor((ib + ie) / 2),  // Comparison index
					cr = time - data[ic][iTime];  // Comparison result

				if(cr < 0)
					ie = ic;
				else if(cr > 0)
					ib = ic + 1;
				else
					return data[ic];
			}
			return ie < data.length ? data[ie] : null;
		}

		// Marker drawing events on mouse movement
		svg
			//.on("mouseover", function showMarker() {
			//	var mouse = d3.mouse(this);
			//	var mX = mouse[0];
			//	if (mX > 0 && mX < width && mouse[1] > 0)                    
			//		marker.style('opacity', 1);
			//	else marker.style("opacity", 0);
			//})
			.on('mouseout', function hideMarker() {
				marker.style("opacity", 0);
				curvals.forEach(function reset(e) { e.text(""); });
				curtime.text("");
			})
			.on('mousemove', function moveMarker() {
				var mouse = d3.mouse(this);
				var mX = Math.round(mouse[0]),
					mY = Math.round(mouse[1]);
				marker.attr('x1', mX).attr('x2', mX);
				if (mX > 0 && mX < width && mY > 0) {
					marker.style('opacity', 1)
						.attr({x1: mX, x2: mX});
					var time = Math.round(trange[0] + (trange[1] - trange[0]) * mX / width);
					//curval.text(axisCapt[1].concat(": ", d3.format(",.0f")(closest(time, 0)[1])));
					curvals.forEach(function caption(e, i) {
						i += 1;
						e.text(axisCapt[i].concat(": ", d3.format(",.0f")(closest(time, 0)[i])));
					});
					curtime.text(d3.time.format("%y-%b-%d")(new Date(time)));
				}
			})
			;

		// Draw axes
		svg.append("g")
			.attr("class", "axis")
			.attr("transform", "translate(0,".concat(height, ")"))
			.call(xAxis)
			;
		svg.append("g")
			.attr("class", "axis")
			.call(yAxis)
			;

		// Draw the chart
		lines.forEach(function(line, i) {
			svg.append("path")
				.datum(data)
				.attr("class", "line ".concat("s", i + 1))
				.attr("d", line)
				;
		});
	}
	// Charts Adapters =========================================================

	// Main executable /////////////////////////////////////////////////////////////
	//new JsonHandler("data/topusers.json", drawTopusers);

	var pdata;
	pdata = [["users", "frequency"], [["anonimous93", 9128], ["mkunt4", 8751], ["globalgrind", 5050],	["greenliberation", 4658],
		["huffpostgreen", 4649], ["carbongate", 4473], ["metoffice", 4284], ["danjweiss", 4260], ["qz", 3486], ["gregdunham75", 2614]]];
	var pndata = [["sentiment", "opinion"], [["global warming", -10], ["polution", -25], ["alternative energy", 20], ["ocean", 13], ["Earth", 35]]];
	var tdata = [["count","time"],[[93580,1389484800000],[216333,1389571200000],[327143,1389657600000],
[267250,1389744000000],[120629,1389830400000],[55926,1389916800000],
[49082,1390003200000],[48572,1390089600000],[52285,1390176000000],
[54687,1390262400000],[13961,1390348800000],[2932,1390435200000],
[33389,1390521600000],[49613,1390608000000],[35093,1390694400000],
[31147,1390780800000],[21639,1390867200000],[2029,1390953600000],
[1732,1391040000000],[1875,1391126400000],[901,1391212800000],
[844,1391299200000],[2124,1391385600000],[2205,1391472000000],
[3141,1391558400000],[42826,1391644800000],[62877,1391731200000],
[45976,1391817600000]]];

	// Assign main drawing function to the window property
	Object.defineProperty(this, "drawAll", {
		value: function() {
			drawHorizScoreBar(10, 10, "#tweet_score");
			drawScorePie(10, 10, "#art_score");
			//drawHorizSimpleBar(pdata, "#storiespi", 0, 1);
			//drawHorizPosNegBar(pndata, "#sentiments", 0, 1);
			drawTimeSeries(tdata, "#topics");
			//drawWorldMap();
		}
	});
	
	// Assign main drawing function to the window property
	Object.defineProperty(this, "draw", {
		value: function(id, func, a1, a2, url) {
			if(url) 
				new JsonHandler(url, this[func], id, a1, a2);
			else this[func](a1, a2, id);
		}
	});
}

/// Define global variable for the charts -------------------------------------
var climoCharts = new ClimoCharts();
// charts.drawAll();

// Exporting charts drawing functions
function climoChart(id, func, a1, a2, url) {
	climoCharts.draw(id, func, a1, a2, url); 
}
