/** BlockVotes (Topic) Object with counters*/
function BlockVotes(id, twp, twn, arp, arn) {
	this.id = id;
	this.twp = twp;
	this.twn = twn;
	this.arp = arp;
	this.arn = arn;
}

//* Global topic sections counter */
// This data should be fetched from the global window.block
//var votes = new BlockVotes(1, 46, 12, 25, 4);

/** Update visual counters
* tw - tweets voting counters
* ar - articles voting counters
*/
function cntUpdate(tw, ar) {
	var votes = window.block.data.votes[window.block.index];

	if(tw == null && ar == null) {
		tw = true;
		ar = true;
	}

	var e = d3.select(".tweet_score");
	if(tw) {
		e.select(".pos").text(votes.twp);
		e.select(".neg").text(votes.twn);
		climoChart("#tweet_score", "drawHorizScoreBar", votes.twp, votes.twn);
	}

	if(ar) {
		e = d3.select(".believe_dont");
		climoChart("#art_score", "drawScorePie", votes.arp, votes.arn);
		var sum = votes.arp + votes.arn;
		var npos = Math.round(votes.arp * 100 / sum);
		e.select(".pos").text(npos + "%");
		e.select(".neg").text((100 - npos) + "%");
	}
}


// JQuery routines
jQuery(document).ready(function ($) {

//-----------------------slider for debates---------------------------------------------------------------------------------//
   var sliderBlock = $('div.slider-debates').children('div.slider_children_debates'),
        blockWidth = sliderBlock.find('div.debates_content').first().width(),
        blockLen = sliderBlock.find('div.debates_content').length,
        current = 1,
        totalBlocksWidth = blockLen * blockWidth;

   $('img.slider_cntr_debates').on('click', function() {
       var direction = $(this).data('dir'),
            loc = blockWidth;

        // update current value
        ( direction === 'next' ) ? ++current : --current;

        // if first image
        if ( current === 0 ) {
            current = blockLen;
            loc = totalBlocksWidth - blockWidth;
            direction = 'next';
        } else if ( current - 1 === blockLen ) {
           current = 1;
           loc = 0;
       }

       transition(sliderBlock, loc, direction);
   });


    function transition( container, loc, direction ) {
        var unit; // -= +=

        if ( direction && loc !== 0 ) {
            unit = ( direction === 'next' ) ? '-=' : '+=';
        }

        container.animate({
            'margin-left': unit ? (unit + loc) : loc
        });
    }

//	/** Draw charts */
//	var charts = new ClimoCharts();
//	charts.drawAll();
//	climoChart("#tweet_score", "drawHorizScoreBar", 15, 10);
//	climoChart("#art_score", "drawScorePie", 15, 10);

	// Update visual counters
	cntUpdate();
});

// Slider handlers --------------------------------------------------------
/** Updare Block Content
*/
function activate(e) {
	d3.select(".".concat(e.className, ".active")).classed("active", false);
	e = d3.select(e);
	e.classed("active", true);
	// Update the content
	// Block index
	var iblock = e.attr("iblock");
	window.block.index = iblock;
	
	// Reload votes counters onsidering target topic
	cntUpdate();

	var data = window.block.data;
	var dart = data.topics[iblock];
	// Block name
	d3.select("#block_name")
		.text(dart.name);
	// Tweet content
	var dtweet = data.tweets[iblock];
	var tweet = d3.select(".tweet");
	tweet.select("img.person").attr("src", "static/img/" + dtweet.photo);
	tweet.select("#auth_name").text(dtweet.name);
	tweet.select("#auth_nick").text(dtweet.screen_name);
	tweet.select(".lead").text(dtweet.text);
	// Article content
	var art = d3.select("#art_content");
	art.select("#art_header").text(dart.heading);
	art.select("img").attr("src", "static/img/Topic/" + dart.photo);
	art.select("#post_content").text(dart.description);
}
// Score buttons handlers -------------------------------------------------
/** Updates image source if required
* e - html image element
* fname - filename of the image (without the path)
*/
function imgUpdate(e, fname) {
	if(e.src.length < fname) {
		e.src = "static/img/" + fname;
		return;
	}
	for(var i = 1; i <= fname.length; ++i)
		if(fname[fname.length - i] != e.src[e.src.length - i]) {
			e.src = "static/img/" + fname;
			return;
		}
}

/** Voting funciton
* e - voting item: {t - tweet, a - article, d - debate}
* v - value: {-1, 1}
*/
function vote(e, v) {
	var votes = window.block.data.votes[window.block.index];
	switch(e) {
	case 't':
		if(v == 1)
			++votes.twp;
		else ++votes.twn;
		cntUpdate(true);
		break;
	case 'a':
		if(v == 1)
			++votes.arp;
		else ++votes.arn;
		cntUpdate(false, true);
		break;
	case 'd':
		break;
	}
}
